#!/bin/bash

# VTK-m master - Apr 5, 2021
VTKM_HASH=4df064f3793eaf41c2c273c87a226578440772b0
VTKM_SHORT_HASH=${VTKM_HASH:0:8}

# ADIOS2 master - Jan 7, 2021
ADIOS_HASH=bf3a13d9f76983341de83c544a82f0bd58eb2ff8
ADIOS_SHORT_HASH=${ADIOS_HASH:0:8}

cp install_cmake.sh ubuntu18
cp install_adios2.sh ubuntu18
cp install_vtkm.sh ubuntu18
IMAGE_NAME=kitware/vtk:ci-fides-vtkm${VTKM_SHORT_HASH}-adios${ADIOS_SHORT_HASH}-latest
docker image build --build-arg VTKM_HASH=$VTKM_HASH \
  --build-arg ADIOS_HASH=$ADIOS_HASH \
  -t $IMAGE_NAME \
  ubuntu18/
rm ubuntu18/install_adios2.sh
rm ubuntu18/install_vtkm.sh
rm ubuntu18/install_cmake.sh
docker push $IMAGE_NAME

cp install_cmake.sh ubuntu18-ompi
cp install_adios2.sh ubuntu18-ompi
cp install_vtkm.sh ubuntu18-ompi
IMAGE_NAME=kitware/vtk:ci-fides-vtkm${VTKM_SHORT_HASH}-adios${ADIOS_SHORT_HASH}-ompi-latest
docker image build --build-arg VTKM_HASH=$VTKM_HASH \
  --build-arg ADIOS_HASH=$ADIOS_HASH \
  -t $IMAGE_NAME \
  ubuntu18-ompi/
rm ubuntu18-ompi/install_adios2.sh
rm ubuntu18-ompi/install_vtkm.sh
rm ubuntu18-ompi/install_cmake.sh
docker push $IMAGE_NAME

#edit names in .gitlab-ci.yml
sed -i -e "s/ci-fides-vtkm[a-zA-Z0-9]\{8\}-adios[a-zA-Z0-9]\{8\}/ci-fides-vtkm${VTKM_SHORT_HASH}-adios${ADIOS_SHORT_HASH}/g" ../../../.gitlab-ci.yml
