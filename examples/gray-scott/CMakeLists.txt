if (FIDES_USE_MPI)
  add_executable(gray-scott
    simulation/main.cpp
    simulation/gray-scott.cpp
    simulation/settings.cpp
    simulation/writer.cpp
  )
  target_link_libraries(gray-scott adios2::adios2 MPI::MPI_C MPI::MPI_CXX fides)

  add_executable(fides-sst-reader simulation/fides-sst-reader.cpp)
  target_link_libraries(fides-sst-reader PUBLIC
    adios2::adios2 vtkm_cont vtkm_rendering MPI::MPI_C MPI::MPI_CXX fides)

  # copy the files necessary for running example to the build dir
  configure_file(adios2-fides.xml "${CMAKE_CURRENT_BINARY_DIR}" COPYONLY)
  configure_file(fides-gray-scott.json "${CMAKE_CURRENT_BINARY_DIR}" COPYONLY)
  configure_file(settings-staging.json "${CMAKE_CURRENT_BINARY_DIR}" COPYONLY)
  configure_file(gs-vis.py "${CMAKE_CURRENT_BINARY_DIR}" COPYONLY)
endif()
