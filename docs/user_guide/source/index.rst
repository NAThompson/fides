.. Fides documentation master file, created by
   sphinx-quickstart on Sat Jul 11 14:50:21 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Fides User Guide
================

Fides enables complex scientific workflows to seamlessly integrate simulation and visualization.
This is done by providing a data model in JSON that describes the mesh and fields in the data to be read.
Using this data model, Fides maps `ADIOS2 <https://github.com/ornladios/ADIOS2>`_ data arrays (from files or streams) to `VTK-m <https://gitlab.kitware.com/vtk/vtk-m>`_ datasets, enabling visualization of the data using shared- and distributed-memory parallel algorithms.



.. toctree::
   :caption: Setting Up

   setting_up/setting_up


.. toctree::
   :caption: Basics

   components/components
   api/api

.. toctree::
   :caption: Fides reader in ParaView

   paraview/paraview
